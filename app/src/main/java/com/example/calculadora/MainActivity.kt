package com.example.calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var textView: TextView? = null
    private var num1: Int = 0
    private var num2: Int = 0
    private var symb: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.textView = findViewById<TextView>(R.id.textView2).apply {
            text="0"
        }
        this.num1 = 0
        this.num2 = 0
        this.symb = 0
    }

    fun buttonPress(view: View){
        /*
        textView?.apply {
            text= view.tag.toString();
        }
        */
        var buttonID: String=view.tag.toString()
        if(buttonID=="C"){
            this.textView?.apply {
                text="0";
            }
            if(this.symb==0){
                this.num1=0;
            }
            else{
                this.num2=0;
            }
        }
        else if(buttonID=="plus" || buttonID=="minus" || buttonID=="div" || buttonID=="mult") {
            if(this.symb!=0){
                if(this.num2!=0){
                    this.num1=equal().toInt()
                }
            }
            if(buttonID=="plus"){
                this.symb=1
            }
            if (buttonID=="minus"){
                this.symb=2
            }
            if (buttonID=="mult"){
                this.symb=3
            }
            if (buttonID=="div"){
                this.symb=4
            }
        }
        else if(buttonID=="="){
            equal()
        }
        else{
            if(this.symb==0){
                if(!isOverflow(this.num1,10,3)){
                    if(!isOverflow(this.num1*10,1,1)){
                        this.num1=(this.num1*10)+buttonID.toInt()
                    }
                }
                this.textView?.text=this.num1.toString()
            }
            else{
                if(!isOverflow(this.num2,10,3)){
                    if(!isOverflow(this.num2*10,1,1)){
                        this.num2=(this.num2*10)+buttonID.toInt()
                    }
                }
                this.textView?.text=this.num2.toString()
            }
        }
    }

    fun equal(): String{
        var ret="";
        if(this.symb==0){
            ret=this.num1.toString()
        }
        else if(this.symb==1){
            if(!isOverflow(this.num1,this.num2,this.symb)){
                ret=(this.num1+this.num2).toString()
            }
            else{
                ret="Size Error"
            }
        }
        else if(this.symb==2){
            if(!isOverflow(this.num1,this.num2,this.symb)){
                ret=(this.num1-this.num2).toString()
            }
            else{
                ret="Size Error"
            }
        }
        else if(this.symb==3){
            if(!isOverflow(this.num1,this.num2,this.symb)){
                ret=(this.num1*this.num2).toString()
            }
            else{
                ret="Size Error"
            }
        }
        else if(this.symb==4){
            if(this.num2==0){
                ret="Math Error"
            }
            else{
                ret=(this.num1/this.num2).toString()
            }
        }
        else{
            ret="No se supone que esto pasara"
        }
        this.symb=0;
        this.num2=0;
        this.num1=0;
        this.textView?.text=ret
        return ret
    }

    fun isOverflow(term1: Int, term2: Int, operation:Int): Boolean{
        var ret: Boolean = true;
        var result: Int = 0;

        if(operation==1){
            result=term1+term2
            if(term1==result-term2){
                ret=false;
            }
            else{
                ret=true;
            }
        }
        if(operation==2){
            result=term1-term2;
            if(term1==result+term2){
                ret=false;
            }
            else{
                ret=true;
            }
        }
        if(operation==3){
            result=term1*term2;
            if(term1==result/term2){
                ret=false;
            }
            else{
                ret=true;
            }
        }
        return ret
    }
}